//这个demo用来验证unique_ptr不可以互相赋值，但是可以使用右值赋值
#include <iostream>
#include <memory>
int main()
{
    std::unique_ptr<int> foo;
    std::unique_ptr<int> bar;

    foo = std::unique_ptr<int>(new int(101)); //rvalue;

    std::cout<<"foo: ";
    if(foo)
        std::cout<<*foo<<std::endl;
    else
        std::cout<<"empty\n";

    bar = std::move(foo);  //using std::move 构造右值

    std::cout<<"foo: ";
    if(foo)
        std::cout<<*foo<<std::endl;
    else
        std::cout<<"empty\n";

    std::cout<<"bar: ";
    if(bar)
        std::cout<<*bar<<std::endl;
    else
        std::cout<<"empty\n";

    return 0;
}

