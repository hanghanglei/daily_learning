#include <iostream>
#include <memory>
using namespace std;

int main()
{
    shared_ptr<int> sp1,sp2;
    weak_ptr<int> wp;

    sp1 = make_shared<int> (20);
    wp = sp1;

    sp2 = wp.lock();
    sp1.reset();
    cout<<"sp1.use_count: "<<sp1.use_count()<<endl;

    sp1 = wp.lock();
    cout<<"sp1.use_count: "<<sp1.use_count()<<endl;

    cout<<"*sp1: "<<*sp1<<endl;
    cout<<"*sp2: "<<*sp2<<endl;
    return 0;
}

