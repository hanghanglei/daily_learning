#include<iostream>
#include<memory>

int main(){
    std::shared_ptr<int> foo = std::make_shared<int>(10);
    // same as;
    std::shared_ptr<int> foo2(new int(10));

    auto bar = std::make_shared<int>(20);

    auto baz = std::make_shared<std::pair<int,int>>(30,40);

    std::cout<<"*foo: "<<*foo << std::endl;
    std::cout<<"*bar: "<<*bar<< std::endl;
    std::cout<<"*baz: "<<baz->first<< std::endl;

    // shared_ptr也重载了operator () ，作用是检测当前shared_ptr是否是一个null指针对象
    
    std::shared_ptr<int> foo3;

    if(foo3)
        std::cout<<"foo points to "<<*foo<<std::endl;
    else
        std::cout<<"foo is null" << std::endl;
    
    //unique函数判断是否是唯一引用
    
    std::shared_ptr<int> foo4(new int(4));

    if(foo4.unique())
        std::cout<<"foo4 is one"<<std::endl;
    else
        std::cout<<"foo4 is not one"<<std::endl;

    //get成员函数能获得元素原生指针
    int* p = new int(5);
    std::shared_ptr<int> foo5(p);

    if(foo5.get()==p)
        std::cout<<"foo5 and p point to the same location"<< std::endl;

    //reset函数放弃对象对指针的拥有权
    foo5.reset();
    if(foo5)
        std::cout<<"reset()不成功"<<std::endl;
    else
        std::cout<<"reset()成功"<<std::endl;
}
