#include <iostream>
using namespace std;
class A
{
public:
    A():a(10)
    {
        func();
    }
public:
    virtual void func()
    {
        cout<<a<<endl;
    }
private:
    int a;
};

class B:public A
{
public:
    B():b(11)
    {
        func();
    }
    virtual void func()
    {
        cout<<b<<endl;
    }
    void func2()
    {
        cout<<b+1<<endl;
    }

private:
    int b;
};

int main()
{
    A* b = new B();
    b->func2();
    
    cout<<endl;
    A* a = new A();

    cout<<endl;
    B* b2 = new B();
    b2->func2();
    return 0;
}

