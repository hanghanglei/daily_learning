# 友元

## 1.分类

> 友元分为友元函数和友元类。友元提供了一种突破封装的方式，有时提供了遍历，但是友元会增加耦合度，破坏封装，所以友元不宜多用。

## 2.作用

> 突破类的封装，可以在类外访问类中成员变量。友元类的所有成员函数都可以是另一个类的友元函数，都可以访问另一个类中的非公有成员。

## 3.使用

> 1. 友元函数：
>    * 使用friend关键字在类中声明函数。
>    * 友元函数没有隐藏的this指针参数。
>    * 友元函数不是类的成员函数，因此不能被继承。
>    * 友元函数不能直接访问类的成员，只能通过对象访问。
>    * 调用友元函数，需要在实际参数中指出要访问的对象。
> 2. 友元类
>    * 友元类的所有成员函数都是另一个类的友元函数->都可以访问另一个类中的非公有成员。
>    * 友元关系是单向的，不具有交换性。
>    * 友元关系不能传递。

## 4.总结

> 友元的使用一般是用在输入输出运算符的重载函数，保证了运算符使用的形式。其他情况下不建议使用友元，因为破坏了类的封装性，增加了代码的耦合度。

