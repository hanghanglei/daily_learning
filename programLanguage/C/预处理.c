/*************************************************************************
*File Name: 预处理.c
*Author: Leihang
*mail: 973331055@qq.com
*Created Time: Wed 19 Aug 2020 04:02:18 AM CST
*describe: 用来实验预处理定义的宏符号
************************************************************************/
#include <stdio.h>

int main()
{
    printf("源文件名：%s\n",__FILE__);
    printf("当前行号：%d\n",__LINE__);
    printf("编译日期：%s\n",__DATE__);
    printf("编译时间：%s\n",__TIME__);
    printf("1:%d\n",__STDC__);
    return 0;
}

