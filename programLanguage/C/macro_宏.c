/*************************************************************************
*File Name: macro_宏.c
*Author: Leihang
*mail: 973331055@qq.com
*Created Time: Wed 19 Aug 2020 04:24:17 AM CST
*describe: 用来复习宏定义的使用
************************************************************************/
#include <stdio.h>
#define FUNC(x) (x+x)
//宏定义可以包含其他的宏，但是不能出现递归，因为他是在预处理阶段进行替换的，不会根据参数判断递归出口
int main()
{
    printf("out is :%d",FUNC(5));
    return 0;
}

