/*************************************************************************
*File Name: cstdarg.c
*Author: Leihang
*mail: 973331055@qq.com
*Created Time: Wed 19 Aug 2020 03:36:00 AM CST
*describe: 了解可变参数的使用
************************************************************************/
#include <stdio.h>
#include <stdarg.h>
int func(int num,...)
{
    va_list valist;
    int sum=0;
    int i;

    //为num个参数初始化 valist
    va_start(valist,num);

    //访问所有赋给valist的参数
    for(i = 0;i < num; i++)
    {
        //每次访问就是一个参数
        sum+=va_arg(valist,int);
    }

    //清理为valist保留的内存
    va_end(valist);
    //C需要手动清理，否则造成内存泄漏

    return sum;
}
int main()
{
    printf("out is:%d",func(3,1,2,3));
    return 0;
}

