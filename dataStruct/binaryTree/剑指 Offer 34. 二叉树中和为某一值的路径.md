#### [剑指 Offer 34. 二叉树中和为某一值的路径](https://leetcode-cn.com/problems/er-cha-shu-zhong-he-wei-mou-yi-zhi-de-lu-jing-lcof/)

难度中等69收藏分享切换为英文关注反馈

输入一棵二叉树和一个整数，打印出二叉树中节点值的和为输入整数的所有路径。从树的根节点开始往下一直到叶节点所经过的节点形成一条路径。

 

**示例:**
给定如下二叉树，以及目标和 `sum = 22`，

```
              5
             / \
            4   8
           /   / \
          11  13  4
         /  \    / \
        7    2  5   1
```

返回:

```
[
   [5,4,11,2],
   [5,8,4,5]
]
```

 

**提示：**

1. `节点总数 <= 10000`

递归写法

```c++
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> vv;
    vector<vector<int>> pathSum(TreeNode* root, int sum) {
        vector<int> arr;
        pathSum(root,sum,arr,0);
        return vv;
    }
    void pathSum(TreeNode* root, int sum,vector<int>& arr,int current)
    {
        if(root==NULL)
            return;
        arr.push_back(root->val);
        current+=root->val;
        if(root->left==NULL&&root->right==NULL)
        {
            if(current==sum)
                vv.push_back(arr);
            arr.pop_back();//到叶子结点返回
            return;
        }
        pathSum(root->left,sum,arr,current);
        pathSum(root->right,sum,arr,current);
        arr.pop_back();
    }
};
```

