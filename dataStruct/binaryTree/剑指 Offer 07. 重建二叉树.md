#### [剑指 Offer 07. 重建二叉树](https://leetcode-cn.com/problems/zhong-jian-er-cha-shu-lcof/)

难度中等169收藏分享切换为英文关注反馈

输入某二叉树的前序遍历和中序遍历的结果，请重建该二叉树。假设输入的前序遍历和中序遍历的结果中都不含重复的数字。

 

例如，给出

```
前序遍历 preorder = [3,9,20,15,7]
中序遍历 inorder = [9,3,15,20,7]
```

返回如下的二叉树：

```
    3
   / \
  9  20
    /  \
   15   7
```

 

**限制：**

```
0 <= 节点个数 <= 5000
```

```c++
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) {
        int len=inorder.size();
        preorder1.assign(preorder.begin(),preorder.end());//拷贝
        for(int i=0;i<len;i++) {
            m.insert(make_pair(inorder[i],i));   //中序遍历数组插入哈希map 查找为o(1)
        }
        return recursion(0,0,len-1);
    }

    TreeNode *recursion(int pre_root,int in_left,int in_right) { 
        //根节点先序下标，左子树中序起始下标，右子树中序结束下标
        if(in_left>in_right) return NULL;
        TreeNode *root=new TreeNode(preorder1[pre_root]);
        int temp=m.find(preorder1[pre_root])->second;  //当前根节点的中序下标

        root->left=recursion(pre_root+1,in_left,temp-1);
        root->right=recursion(temp-in_left+1+pre_root,temp+1,in_right);
        //右子树根节点的前序下标=左子树节点数+pre_root 左子树节点数=temp-in_left+1
        return root;
    }

private:
    unordered_map<int,int> m;
    vector<int> preorder1;
};

```

使用unordered_map快速匹配中序数组中对应的先序数组中的位置。递归的难点在于参数的设计，设计成中序左子树的中序起始下标和中序右子树的结束下标，是因为在中序数组中，其根节点左边位置的节点就是其左子树中序遍历的最后一个节点，右边位置的节点就是其右子树中序遍历的第一个节点。