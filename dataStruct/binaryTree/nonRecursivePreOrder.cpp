/*************************************************************************
*File Name: nonRecursivePreOrder.cpp
*Author: Leihang
*mail: 973331055@qq.com
*Created Time: Thu 06 Aug 2020 10:35:36 PM CST
*describe: 这个demo用来复习二叉树的前序非递归遍历方法，使用了栈
************************************************************************/
#include "binaryNode.h"
#include <iostream>
#include <string>
#include <stack>
using namespace std;
//1.当前节点不为空，压栈，当前节点变成其左孩子
//2.当前节点为空，出栈，判断出栈元素右孩子是否为空 
//    1).为空,将当前节点置空，继续出栈
//    2).不为空，压栈当前右孩子节点，将当前节点设置为右孩子的左孩子，继续1
void nonRecursivePreOrder(Node* root)
{
    stack<Node*> sta;
    while(root!=NULL||!sta.empty())
    {
        if(root!=NULL)
        {
            cout<<root->val<<" ";
            sta.push(root);
            root=root->left;
        }
        else
        {
            root=sta.top();
            sta.pop();
            if(root->right!=NULL)
            {
                sta.push(root->right);
                root=root->right->left;
            }
            else
                root=NULL;
        }
    }
}
int main()
{
    
    int arr[] = {4,1,3,2,5};
    Node* root = NULL;
    for(int i=0;i<5;i++)
    {
        root = build(root,arr[i]);
    }
    nonRecursivePreOrder(root);
    return 0;
}

