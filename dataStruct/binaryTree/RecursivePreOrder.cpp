/*************************************************************************
*File Name: RecursivePreOrder.cpp
*Author: Leihang
*mail: 973331055@qq.com
*Created Time: Fri 07 Aug 2020 10:47:41 AM CST
*describe: 
************************************************************************/
#include "binaryNode.h"

//前序遍历
void RecursivePreOrder(Node* root)
{
    if(root!=NULL)
    {
        cout<<root->val<<" ";
    }
    else 
        return;
    RecursivePreOrder(root->left);
    RecursivePreOrder(root->right);
}

int main()
{
    int arr[] = {4,1,3,2,5};
    Node* root = NULL;
    for(int i=0;i<5;i++)
    {
        root = build(root,arr[i]);
    }
    RecursivePreOrder(root);
    return 0;
}

