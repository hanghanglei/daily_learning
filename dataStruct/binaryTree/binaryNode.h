/*************************************************************************
*File Name: binaryNodo.h
*Author: Leihang
*mail: 973331055@qq.com
*Created Time: Thu 06 Aug 2020 11:53:44 PM CST
*describe: 
************************************************************************/
#pragma once
#include<iostream>
using namespace std;
class BinaryTreeNode
{
public:
    BinaryTreeNode(int value = 0)
        :val(value),left(NULL),right(NULL)
    {}
    int val;
    BinaryTreeNode* left;
    BinaryTreeNode* right;
};

typedef BinaryTreeNode Node;

//返回插入数据的节点
Node* build(Node* root,int val)
{
    if(root==NULL)
    {
        root = new Node;
        root->val = val;
        return root;
    }
    if(val<root->val)
        root->left = build(root->left,val);
    else
        root->right = build(root->right,val);
    return root;
}
