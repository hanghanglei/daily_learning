/*************************************************************************
*File Name: demo.cpp
*Author: Leihang
*mail: 973331055@qq.com
*Created Time: Wed 19 Aug 2020 08:49:28 PM CST
*describe: 练习使用boost::filesystem操作文件
************************************************************************/
#include <iostream>
#include <string>
#include <boost/filesystem.hpp>
#define BOOST_FILESYSTEM_DEPRECATED 1
using namespace std;
int main()
{
    //boost::filesystem::path的构造函数不会验证所提供路径的有效性，或检查给定的文件或目录是否存在。
    //因此，boost::filesystem::path甚至可以用无意义的路径来初始化
    //因为boost::filesystem::path只是处理字符串而已，文件系统并没有被访问到
    boost::filesystem::path p1("~/test"); 

    //boost::filesystem::paht提供三种方法以字符串获取一个路径。
    boost::filesystem::path p2("~/daily_learning");
    cout<<p2.string()<<endl;
    cout<<p2.file_string()<<endl;
    cout<<p2.directory_string()<<endl;
    return 0;
}

